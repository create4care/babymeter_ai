# Babymeter AI

## Software Requirements
As our code IDE we used PyCharm:\
[https://www.jetbrains.com/pycharm/](https://www.jetbrains.com/pycharm/)

You can either use Anaconda or PIP from Python to install the different required packages.

We recommend you using Anaconda, you can download it here:\
[https://www.anaconda.com/distribution/](https://www.anaconda.com/distribution/)

For this project you need the following packages:
- numpy
- pandas
- scikit-learn
- keras
- tensorflow
- opencv

You can install these via the command line by running the following commands:
```sh
pip install numpy
pip install pandas
pip install scikit-learn
pip install keras
pip install tensorflow
pip install opencv-python
```

Now open PyCharm and do the following:
- Go to File > Settings > Project > Project Interpreter
- Set the project interpreter to "Python 3.7 Anaconda"
    - if it's not shown within the dropdown, click on 'Show All...'
    - Select "Python 3.7 Anaconda", if it's not available add it like the following:
        - Press the '+' icon
        - Select 'Conda Environment'
        - Locate your Conda environment and set the Python version to 3.7
        - Press OK

## Run the Prediction Pipeline
- Setup your IDE and install the required software
- Open the "PredictionPipeline.py" file
- If you want to input your own file (like stated in that file as well):
    - Place your files inside the "/data/initial" folder
    - Set the "file_left" and "file_right" variables to point to your desired images
        
        Example:\
        Left image is called "left_image.png" and right image is called
        "right_image.png"
    
        Set the variables as follows:
        ```python
        file_left = "left_image.png"
        file_right = "right_image.png"
        ```
    - Execute the file
    - When it's finished, the path to the files of resulting images with the predictions
    drawn onto them have been printed to the console.
    - Open these generated files to view the result.