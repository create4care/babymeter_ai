"""
Edge Detection

The code uses the "to zero (inverse)" threshold to apply edge detection

"""

import cv2
import numpy as np
from PIL import Image


def edge_detection(img):
    # need to convert image to numpy array to use cv2.threshold
    image_copy = np.array(img)

    ret, thresh = cv2.threshold(image_copy, 127, 255, cv2.THRESH_TOZERO_INV)

    # define an RGB color range that you want to filter out of the image
    darkblue_lo = np.array([0, 0, 80])
    darkblue_hi = np.array([0, 0, 140])

    # use mask to cover the specified color range and alter the existing image in "thresh"
    mask1 = cv2.inRange(thresh, darkblue_lo, darkblue_hi)
    thresh[mask1 > 0] = (0, 0, 0)

    yellow_lo = np.array([0, 0, 0])
    yellow_hi = np.array([255, 255, 0])
    mask2 = cv2.inRange(thresh, yellow_lo, yellow_hi)
    thresh[mask2 > 0] = (0, 0, 0)

    purple_lo = np.array([0, 0, 0])
    purple_hi = np.array([255, 0, 255])
    mask3 = cv2.inRange(thresh, purple_lo, purple_hi)
    thresh[mask3 > 0] = (0, 0, 0)

    # convert the array back to a normal image so it can be saved
    return Image.fromarray(thresh)
