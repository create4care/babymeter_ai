input_folder = "../../../data/initial"
output_folder = "../../../data/output"
output_edge_folder = "../../../data/edge"
predictions_folder = "../../../data/predictions"
models_folder = "../../../data/models"

amount_of_input_images = 38
file_reading_mode = "txt"

input_size = 1280, 720
output_size = 128, 72

image_width = output_size[0]
image_height = output_size[1]
input_shape_size = (image_height, image_width, 3)
file_image_mode = "png"
