"""
Data Enlarger (data preprocessing)

The code does the following:
- loop through all images
- load the images
- load the readings
- skip if feet not on image
- save images & readings
- randomize 100 times the images & readings

"""

import json
from os import listdir, mkdir, path
from random import randint

from PIL import Image

from src.EdgeDetection import edge_detection
from src.logic.DataSettings import *
from src.model.Baby import file_to_baby

files = listdir(input_folder)

if not path.isdir(output_folder):
    mkdir(output_folder)

print("files found: " + str(len(files)))


def randomize_rotate_and_crop():
    width = input_size[0]
    height = input_size[1]

    # setup allowed crop scaling
    ref_scale = 1.25
    ref_width = int(width / ref_scale)
    ref_height = int(height / ref_scale)

    # setup 'degrees of freedom'
    x_freedom = width - ref_width
    y_freedom = height - ref_height

    # randomize x, y and rotation
    random_x = randint(0, x_freedom)
    random_y = randint(0, y_freedom)
    random_rot = randint(-15, 15)

    return [random_rot, random_x, random_y, ref_width, ref_height]


def rotate_and_crop(image, x, y, width, height, rot):
    return image \
        .rotate(rot) \
        .crop((x, y, x + width, y + height))


def save_image(image, index, version, name):
    file_name = str(index) + "_" + str(version) + "_" + name

    # create edge image and save
    image_edge = edge_detection(image.copy())
    image_edge.thumbnail(output_size)
    image_edge.save(output_edge_folder + "/" + file_name + ".png", "PNG")

    # make smaller image and save
    image.thumbnail(output_size)
    image.save(output_folder + "/" + file_name + ".png", "PNG")


def save_reading(content, index, version, name):
    file_name = output_folder + "/" + str(index) + "_" + str(version) + "_" + name + "_reading." + file_reading_mode
    f = open(file_name, 'w')
    f.write(json.dumps(content, default=lambda x: x.__dict__))
    f.close()


print("running...")

for i in range(amount_of_input_images):
    # setup file names
    file_left = str(i) + "_left." + "jpg"
    file_right = str(i) + "_right." + "jpg"

    # open images
    img_left = Image.open(input_folder + "/" + file_left)
    img_right = Image.open(input_folder + "/" + file_right)

    # open readings
    baby_left_reading = file_to_baby(input_folder + "/" + str(i) + "_left_reading." + file_reading_mode)
    baby_right_reading = file_to_baby(input_folder + "/" + str(i) + "_right_reading." + file_reading_mode)

    print("- " + str(file_left) + " <=> " + str(file_right))

    # skip if any foot isn't on the image
    if baby_left_reading.feet.left is None or baby_left_reading.feet.right is None or baby_right_reading.feet.left is None or baby_right_reading.feet.right is None:
        print("  skipped")
        continue

    # save images and readings
    save_image(img_left.copy(), i, 0, "nm_left")
    save_image(img_right.copy(), i, 0, "nm_right")
    save_reading(baby_left_reading.downscale().round(), i, 0, "nm_left")
    save_reading(baby_right_reading.downscale().round(), i, 0, "nm_right")

    # iterate 100 times and randomize the images and readings
    for j in range(100):
        v = j + 1

        # setup randomized state
        o_r, o_x, o_y, o_w, o_h = randomize_rotate_and_crop()

        # transform image
        new_img_left = rotate_and_crop(img_left.copy(), o_x, o_y, o_w, o_h, o_r)
        new_img_right = rotate_and_crop(img_right.copy(), o_x, o_y, o_w, o_h, o_r)
        new_baby_left_reading = baby_left_reading.rotate(o_r).crop(o_x, o_y, o_w, o_h).round()
        new_baby_right_reading = baby_right_reading.rotate(o_r).crop(o_x, o_y, o_w, o_h).round()

        # save transformed images and transformed readings
        save_image(new_img_left, i, v, "nm_left")
        save_image(new_img_right, i, v, "nm_right")
        save_reading(new_baby_left_reading, i, v, "nm_left")
        save_reading(new_baby_right_reading, i, v, "nm_right")

print("done!")
