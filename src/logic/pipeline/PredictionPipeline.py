"""
Prediction Pipeline

The code does the following:
- load in all 'initial' images
- load the pre-trained models for head and feet predictions
- make predictions on the images
- draw the predictions/results on the images
- save the resulting images in /data/models/

"""

import numpy as np
from PIL import Image, ImageDraw
from keras.engine.saving import load_model

from src.logic.DataSettings import models_folder, input_folder, output_size

"""
If you want to input specific files instead of
the initial images, do the following:

Change:
file_left = None
file_right = None

To:
file_left = "input_left_file.jpg"
file_right = "input_right_file.jpg"

Make sure these images are placed inside the
/data/initial folder
"""

file_left = None
file_right = None

"""

"""

# initial image range as input
files = range(0, 38)

print("Loading files...")

if (file_left is None) ^ (file_right is None):
    print("Incorrect input files")
    exit(0)

images = []

# load custom input file if input is provided
if file_left is not None and file_right is not None:
    left = Image.open(input_folder + "/" + file_left)
    right = Image.open(input_folder + "/" + file_right)

    pair = [left, right]
    images.append(pair)
else:
    # else just load the initial image range
    for file in files:
        left = Image.open(input_folder + "/" + str(file) + "_left.jpg")
        right = Image.open(input_folder + "/" + str(file) + "_right.jpg")

        pair = [left, right]
        images.append(pair)

print("Loading model...")
model_head_left = load_model(models_folder + "/model_head_left.h5")
model_head_right = load_model(models_folder + "/model_head_right.h5")
model_feet_left = load_model(models_folder + "/model_feet_left.h5")
model_feet_right = load_model(models_folder + "/model_feet_right.h5")

model_head_left.summary()
model_head_right.summary()
model_feet_left.summary()
model_feet_right.summary()


def draw(img, head_pred, feet_pred, scale_factor=10):
    # ellipse
    hx, hy, hrx, hry = head_pred * scale_factor
    flx, fly, flrx, flry, frx, fry, frrx, frry = feet_pred * scale_factor

    draw = ImageDraw.Draw(img)
    draw.ellipse((hx - hrx, hy - hry, hx + hrx, hy + hry), outline="red", width=scale_factor // 2)
    draw.ellipse((flx - flrx, fly - flry, flx + flrx, fly + flry), outline="green", width=scale_factor // 2)
    draw.ellipse((frx - frrx, fry - frry, frx + frrx, fry + frry), outline="blue", width=scale_factor // 2)
    del draw

    # Image._show(img)
    return img


output = []

# loop through image pairs
for img_left, img_right in images:
    image_left = img_left.copy()
    image_right = img_right.copy()

    # make images smaller (prediction size)
    image_left.thumbnail(output_size)
    image_right.thumbnail(output_size)

    image_left_arr = np.array(image_left)
    image_right_arr = np.array(image_right)

    # make predictions
    t_head_left = model_head_left.predict(np.array([image_left_arr]))
    t_head_right = model_head_right.predict(np.array([image_right_arr]))
    t_feet_left = model_feet_left.predict(np.array([image_left_arr]))
    t_feet_right = model_feet_right.predict(np.array([image_right_arr]))
    print(t_head_left)
    print(t_head_right)
    print(t_feet_left)
    print(t_feet_right)

    # add to output drawings
    output.append(draw(img_left, t_head_left[0], t_feet_left[0]))
    output.append(draw(img_right, t_head_right[0], t_feet_right[0]))

# get correct dimensions
widths, heights = zip(*(i.size for i in output))
total_width = min(widths) * 2
max_height = max(heights)

# create final images
for i in range(len(output) // 2):
    new_img = Image.new('RGB', (total_width, max_height))
    x_offset = 0
    for j in range(2):
        im = output[i * 2 + j]
        new_img.paste(im, (x_offset, 9))
        x_offset += im.size[0]

    file_output = models_folder + "/grid_" + str(i) + ".png"
    new_img.save(file_output, "PNG")
    print("Saved image to: " + file_output)
