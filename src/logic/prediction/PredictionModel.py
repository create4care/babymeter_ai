"""
Prediction model

The code does the following:
- setup necessary variables
- loop through body types and image sides
    (make 4 models for head*feet,left*right combinations)
- setup CNN, compile and fit
- predict
- (optional) save images with predictions

"""

from os import listdir, mkdir, path

import numpy as np
from PIL import Image, ImageDraw
from keras.layers import Dense, Conv2D, Flatten
from keras.models import Sequential
from sklearn.model_selection import train_test_split

from src.logic.DataSettings import models_folder, predictions_folder, input_shape_size, image_width, image_height, \
    file_image_mode, output_folder
from src.model import Baby
from src.model.Baby import file_to_baby

"""
settings
"""
test_size = 0.25
number_of_epochs = 10
optimizer = 'adam'
loss_function = 'mean_absolute_error'

body_types = ["head", "feet"]
sides = ["left", "right"]
output_layer_size = 4
save_images_output = True
"""

"""

for body_type in body_types:
    for side in sides:
        print(body_type)
        print(side)
        print("=" * 15)

        print("Setup model...")
        model = Sequential()
        model.add(Conv2D(128, kernel_size=3, activation='softmax', input_shape=input_shape_size))
        model.add(Flatten())
        model.add(Dense(64, activation='relu'))
        model.add(Dense(output_layer_size if body_type == "head" else output_layer_size * 2, activation='relu'))

        print("Getting files...")
        files = listdir(output_folder)


        class Item:
            def __init__(self, file_path: str, file_name: str, image: Image, baby: Baby, feet_only: bool):
                self.file_path = file_path
                self.file_name = file_name
                self.image = np.array(image)
                self.baby = baby.to_array(image_width, image_height, feet_only)


        images = [f for f in files if f.endswith(file_image_mode)]

        print("Loading data...")
        items = []
        for image_file in images:
            image_path = output_folder + "/" + image_file

            # load image
            img = Image.open(image_path)

            # get only filename: e.g. 4_60_nm_left
            f = image_file[0:(len(image_file) - len("." + file_image_mode))]

            # setup baby labels
            bf = file_to_baby(output_folder + "/" + f + "_reading.txt")

            # only use baby if correctly loaded and is current side
            item = Item(image_path, image_file, img, bf, body_type == "feet")
            if item.baby is not None and side in f:
                items.append(item)

        print("Preparing train and test set...")
        np.random.seed(42)
        np.random.shuffle(items)

        X_file_paths = []
        X = []
        y = []
        for item in items:
            X_file_paths.append([item.file_path, item.file_name])
            X.append(item.image)
            y.append(item.baby)

        X = np.array(X)
        y = np.array(y)

        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=test_size)

        print("Compiling...")
        model.compile(optimizer=optimizer, loss=loss_function, metrics=['accuracy'])

        print("Fitting...")
        model.fit(X_train, y_train, validation_data=(X_test, y_test), epochs=number_of_epochs)

        print("Saving model...")
        if not path.isdir(models_folder):
            mkdir(models_folder)
        model.save(models_folder + "/model_" + body_type + "_" + side + ".h5")

        print("Predicting...")
        t = model.predict(X)
        print(t)

        if save_images_output:
            def find_file_path_and_name(data, item):
                index = 0
                for i in data:
                    if np.array_equal(i, item):
                        break
                    index += 1
                return X_file_paths[index]


            if not path.isdir(predictions_folder):
                mkdir(predictions_folder)

            print("Turning the predictions into images...")
            z = zip(X, t)
            for (x, t) in z:
                file_path, file_name = find_file_path_and_name(X, x)
                img = Image.open(file_path)

                """head and feet"""
                # hx, hy, hw, hh, flx, fly, flw, flh, frx, fry, frw, frh = t
                # hx, hy, hrx, hry, flx, fly, flrx, flry, frx, fry, frrx, frry = t

                """head"""
                # rectangle
                # hx, hy, hw, hh = t
                # circle
                # hx, hy, hr = t
                # ellipse
                hx, hy, hrx, hry = t

                """feet"""
                # rectangle
                # flx, fly, flw, flh, frx, fry, frw, frh = t
                # circle
                # flx, fly, flr, frx, fry, frr = t
                # ellipse
                # flx, fly, flrx, flry, frx, fry, frrx, frry = t

                draw = ImageDraw.Draw(img)

                # rectangle
                # draw.rectangle((hx, hy, hx + hw, hy + hh), outline="red", width=1)
                # draw.rectangle((flx, fly, flx + flw, fly + flh), outline="green", width=1)
                # draw.rectangle((frx, fry, frx + frw, fry + frh), outline="blue", width=1)
                # circle
                # draw.ellipse((hx - hr, hy - hr, hx + hr, hy + hr), outline="red", width=1)
                # draw.ellipse((flx - flr, fly - flr, flx + flr, fly + flr), outline="green", width=1)
                # draw.ellipse((frx - frr, fry - frr, frx + frr, fry + frr), outline="blue", width=1)
                # ellipse
                draw.ellipse((hx - hrx, hy - hry, hx + hrx, hy + hry), outline="red", width=1)
                # draw.ellipse((flx - flrx, fly - flry, flx + flrx, fly + flry), outline="green", width=1)
                # draw.ellipse((frx - frrx, fry - frry, frx + frrx, fry + frry), outline="blue", width=1)

                del draw

                img.save(predictions_folder + "/" + body_type + "_" + file_name, "PNG")
