import math

import numpy as np

from src.logic.DataSettings import output_size, input_size


class Area:
    def __init__(
            self,
            x: float,
            y: float,
            top: float,
            right: float,
            bottom: float,
            left: float
    ):
        self.x = x
        self.y = y
        self.top = top
        self.right = right
        self.bottom = bottom
        self.left = left

    def downscale(self, width, height):
        return Area(
            self.x * output_size[0] / width,
            self.y * output_size[1] / height,
            self.top * output_size[1] / height,
            self.right * output_size[0] / width,
            self.bottom * output_size[1] / height,
            self.left * output_size[0] / width
        )

    def rotate(self, rot):
        mid_x = input_size[0] / 2
        mid_y = input_size[1] / 2

        offset_x = self.x - mid_x
        offset_y = self.y - mid_y
        a = rot * math.pi / 180

        new_x = offset_y * math.sin(a) + offset_x * math.cos(a)
        new_y = offset_y * math.cos(a) - offset_x * math.sin(a)

        new_x += mid_x
        new_y += mid_y

        if rot == 0:
            return Area(self.x, self.y, self.top, self.right, self.bottom, self.left)
        elif rot > 0:
            turn = rot / 90
            return Area(
                new_x,
                new_y,
                self.top * (1 - turn) + self.left * turn,
                self.right * (1 - turn) + self.top * turn,
                self.bottom * (1 - turn) + self.right * turn,
                self.left * (1 - turn) + self.bottom * turn
            )
        else:
            turn = -rot / 90
            return Area(
                new_x,
                new_y,
                self.top * (1 - turn) + self.right * turn,
                self.right * (1 - turn) + self.bottom * turn,
                self.bottom * (1 - turn) + self.left * turn,
                self.left * (1 - turn) + self.top * turn
            )

    def crop(self, x, y, width, height):
        return Area(
            self.x - x,
            self.y - y,
            self.top,
            self.right,
            self.bottom,
            self.left
        ).downscale(width, height)

    def round(self):
        return Area(
            int(self.x),
            int(self.y),
            int(self.top),
            int(self.right),
            int(self.bottom),
            int(self.left)
        )

    def to_array(self, image_width, image_height):
        # circle
        # x = int(self.x - self.left / 2 + self.right / 2)
        # y = int(self.y - self.top / 2 + self.bottom / 2)
        # r = int((self.left + self.right) / 4 + (self.top + self.bottom) / 4)
        # if x > image_width or y > image_height:
        #     return None
        # return np.array([x, y, r])

        # ellipse
        x = int(self.x - self.left / 2 + self.right / 2)
        y = int(self.y - self.top / 2 + self.bottom / 2)
        xr = int((self.left + self.right) / 2)
        yr = int((self.top + self.bottom) / 2)
        if x > image_width or y > image_height:
            return None
        return np.array([x, y, xr, yr])

        # rectangle
        # x = int(self.x - self.left)
        # y = int(self.y - self.top)
        # w = int(self.left + self.right)
        # h = int(self.bottom + self.top)
        # if x > image_width or y > image_height:
        #     return None
        # return np.array([x, y, w, h])

        # complex
        # return np.array([
        #     self.x,
        #     self.y,
        #     self.top,
        #     self.right,
        #     self.bottom,
        #     self.left
        # ])
