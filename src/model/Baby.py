import json

import numpy as np

from src.logic.DataSettings import input_size
from src.model.Area import Area
from src.model.Feet import Feet


class Baby:
    def __init__(self, head: Area, feet: Feet):
        self.head = head
        self.feet = feet

    def downscale(self, width=input_size[0], height=input_size[1]):
        return Baby(self.head.downscale(width, height), self.feet.downscale(width, height))

    def rotate(self, rot):
        return Baby(self.head.rotate(rot), self.feet.rotate(rot))

    def crop(self, x, y, width, height):
        return Baby(self.head.crop(x, y, width, height), self.feet.crop(x, y, width, height))

    def round(self):
        return Baby(self.head.round(), self.feet.round())

    def to_array(self, image_width, image_height, feet_only):
        head = self.head.to_array(image_width, image_height)
        feet = self.feet.to_array(image_width, image_height) if feet_only else []

        # both must exist
        if head is None or feet is None:
            return None

        if not feet_only:
            return np.concatenate([
                head,
                feet
            ])
        else:
            return np.concatenate([
                feet
            ])


def file_to_baby(p):
    f = open(p, 'r')
    js = json.load(f)

    # setup head
    head_json = js["head"]

    head = Area(
        head_json["x"],
        head_json["y"],
        head_json["top"],
        head_json["right"],
        head_json["bottom"],
        head_json["left"]
    )

    # setup feet
    feet_json = js["feet"]
    feet_left_json = feet_json["left"]
    feet_right_json = feet_json["right"]

    if feet_left_json is not None:
        left = Area(
            feet_left_json["x"],
            feet_left_json["y"],
            feet_left_json["top"],
            feet_left_json["right"],
            feet_left_json["bottom"],
            feet_left_json["left"]
        )
    else:
        left = None

    if feet_right_json is not None:
        right = Area(
            feet_right_json["x"],
            feet_right_json["y"],
            feet_right_json["top"],
            feet_right_json["right"],
            feet_right_json["bottom"],
            feet_right_json["left"]
        )
    else:
        right = None

    feet = Feet(left, right)

    return Baby(head, feet)
