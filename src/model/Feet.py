import numpy as np

from src.model import Area


class Feet:
    def __init__(self, left: Area, right: Area):
        self.left = left
        self.right = right

    def downscale(self, width, height):
        if self.left is not None:
            left = self.left.downscale(width, height)
        else:
            left = None

        if self.right is not None:
            right = self.right.downscale(width, height)
        else:
            right = None

        return Feet(left, right)

    def rotate(self, rot):
        if self.left is not None:
            left = self.left.rotate(rot)
        else:
            left = None

        if self.right is not None:
            right = self.right.rotate(rot)
        else:
            right = None

        return Feet(left, right)

    def crop(self, x, y, width, height):
        if self.left is not None:
            left = self.left.crop(x, y, width, height)
        else:
            left = None

        if self.right is not None:
            right = self.right.crop(x, y, width, height)
        else:
            right = None

        return Feet(left, right)

    def round(self):
        if self.left is not None:
            left = self.left.round()
        else:
            left = None

        if self.right is not None:
            right = self.right.round()
        else:
            right = None

        return Feet(left, right)

    def to_array(self, image_width, image_height):
        if self.left is not None:
            left = self.left.to_array(image_width, image_height)
        else:
            left = None

        if self.right is not None:
            right = self.right.to_array(image_width, image_height)
        else:
            right = None

        if left is None or right is None:
            return None
        return np.concatenate([left, right])
